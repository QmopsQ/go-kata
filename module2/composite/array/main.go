package main

import "fmt"

func main() {
	a := [...]int{34, 55, 89, 144}
	fmt.Println("original value", a)
	a[0] = 21
	fmt.Println("changed first element", a)
	b := a
	a[0] = 233
	fmt.Println("original array", a)
	fmt.Println("modified array", b)

}

type User struct {
	Age    int
	Name   string
	Wallet Wallet
}
type Wallet struct {
	RUR uint64
	USD uint64
	BTC uint64
	ETH uint64
}

func rangeArray() {
	users := [4]User{
		{Age: 8,
			Name: "John",
			Wallet: Wallet{
				RUR: 13,
				USD: 1,
				BTC: 0,
				ETH: 0,
			},
		},
		{
			Age:  13,
			Name: "Katie",
			Wallet: Wallet{
				RUR: 500,
				USD: 3,
				BTC: 0,
				ETH: 0,
			},
		},
		{
			Age:  21,
			Name: "Doe",
			Wallet: Wallet{
				RUR: 0,
				USD: 300,
				BTC: 1,
				ETH: 3,
			},
		},
		{
			Age:  34,
			Name: "Arnie",
			Wallet: Wallet{
				RUR: 987342,
				USD: 34,
				BTC: 1,
				ETH: 3,
			},
		},
	}
	fmt.Println("users older 18:")
	for i := range users {
		if users[i].Age > 18 {
			fmt.Println(users[i])
		}
	}

}
