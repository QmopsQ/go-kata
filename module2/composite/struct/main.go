package main

import (
	"fmt"
	"unsafe"
)

type User struct {
	Age    int
	Name   string
	Wallet Wallet
}

func main() {
	user := User{
		Age:  13,
		Name: "Alexander",
	}
	fmt.Println(user)
	wallet := Wallet{
		RUR: 250000,
		USD: 3500,
		BTC: 1,
		ETH: 4,
	}
	fmt.Println(wallet)
	fmt.Println("wallet allocates: ", unsafe.Sizeof(wallet), "bytes")
	user.Wallet = wallet
	fmt.Println(user)
}

type Wallet struct {
	RUR uint64
	USD uint64
	BTC uint64
	ETH uint64
}
